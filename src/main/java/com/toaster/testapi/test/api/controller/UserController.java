package com.toaster.testapi.test.api.controller;

import com.toaster.testapi.test.api.models.Users;
import com.toaster.testapi.test.api.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/all")
    public ResponseEntity<List<Users>> getAllUsers() {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUsersById(@PathVariable Long id) {
        if (userRepository.findById(id).isPresent()) {
            return new ResponseEntity<>(userRepository.findById(id).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>("User not found!", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/")
    public ResponseEntity<?> getUsersByName(@RequestParam String name) {
        if (userRepository.findUsersByName(name).isPresent()) {
            return new ResponseEntity<>(userRepository.findUsersByName(name).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>("User not found!", HttpStatus.NOT_FOUND);
    }
}
