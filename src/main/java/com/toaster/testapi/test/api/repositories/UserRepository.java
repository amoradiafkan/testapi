package com.toaster.testapi.test.api.repositories;

import com.toaster.testapi.test.api.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.DoubleStream;

@Repository
public interface UserRepository extends JpaRepository<Users,Long> {
    Optional<Users> findUsersByName(String name);
}
